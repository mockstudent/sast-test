import os

aws_secret_access_key = os.env['AWS_SECRET_ACCESS_KEY'] # This isn't real it was randomly generated using https://1password.com/password-generator/

def demo():
    print("This is random code that is only here for demo purposes and will not be used.")
